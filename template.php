<?php

/**
 * @file
 * Here we override the default HTML output of drupal.
 */

/**
 * Implements theme_status_messages().
 *
 * Use version in uw_core_theme. Using hook_theme() to do this does not work
 * properly: The messages are set too late and appear on the next page load.
 */
function uw_admin_theme_status_messages($variables) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('theme', 'uw_core_theme') . '/template.php';
  return uw_core_theme_status_messages($variables);
}
